# README #

### What is this repository for? ###

* This plugin is only for CSGO Community Servers

### How do I get set up? ###

* upload the .cfg file to addons/sourcemod/configs
* upload the .smx file to addons/sourcemod/plugins
* upload the .sp file to addons/sourcemod/scripting

### Contribution guidelines ###

* Follow the guide from "How do I get set up?"

### Who do I talk to? ###

* If you have any problems or questions please contact Marko on Steam "http://steamcommunity.com/profiles/76561198312713613"
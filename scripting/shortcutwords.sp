#include <sourcemod>

#define DATA "1.5.3"

static String:wordspath[PLATFORM_MAX_PATH];

static String:Words[100][2][50];

public Plugin:myinfo = {
    name = "[SM] Shortcut Words",
	author = "Marko M4rk0",
	description = "",
	version = DATA,
	url = "http://steamcommunity.com/profiles/76561198312713613"
};

public OnPluginStart()
{
	BuildPath(Path_SM, wordspath, sizeof(wordspath), "configs/shortcutwords.cfg");
	RegConsoleCmd("say", commandsay, "");
}

public OnMapStart()
{
    LoadWords();
}

public Action commandsay(client, args)
{
    decl String:arg1[200];
	
	GetCmdArg(1, arg1, sizeof(arg1));
	
	for(new i = 0;i < sizeof(Words);i++)
	{
	    if(!StrEqual(Words[i][0], "") && !StrEqual(Words[i][1], ""))
		{
		    if(StrContains(arg1, Words[i][0], false) != -1)
			{
			    ReplaceString(arg1, sizeof(arg1), Words[i][0], Words[i][1], false);
				decl String:name[32];
				GetClientName(client, name, 32);
				PrintToChatAll("%s: \x5%s", name, arg1);
				return Plugin_Handled;
			} else continue;
		} else continue;
	}
	
	return Plugin_Continue;
}

public LoadWords()
{
    new Handle:file = OpenFile(wordspath, "rt");
    
    if(file == INVALID_HANDLE)
    {
	    PrintToServer("Cannot load the words");
		return;
	}
    
    new String:linedata[50], String:buffer[2][50], i = 0, length;
    
    while(!IsEndOfFile(file) && ReadFileLine(file, linedata, sizeof(linedata)))
   {
		length = strlen(linedata);
		
		if(linedata[length - 1] == '\n')
		    linedata[--length] = '\0';
			
	    if(linedata[0] != '\\' && linedata[1] != '\\')
		{
		    if(StrContains(linedata, "Match:", false) != -1)
			{
			    ExplodeString(linedata, ":", buffer, 2, 50);
				
				Words[i][0] = buffer[1];
			} else if(StrContains(linedata, "Replace:", false) != -1)
			{
			    ExplodeString(linedata, ":", buffer, 2, 50);
				
				Words[i][1] = buffer[1];
			} else if(StrEqual(linedata, "End;"))
			{
			    i++;
			}
		} else continue;
	}

    if(i == 0)
    {
	    PrintToServer("The file is blank");
	}
	
	CloseHandle(file);
}